// External dependencies
import { max } from 'd3-array'
import { axisBottom, axisLeft } from 'd3-axis'
import { scaleLinear, scaleTime } from 'd3-scale'
import { select } from 'd3-selection'
import { timeParse } from 'd3-time-format'

// Internal dependencies
import './style.css'
import dataJSON from './data.json'

const data = dataJSON.data as Array<[string, number]>

const height = 400
const width = 1000
const padding = 20
const leftPadding = 50
const rangeYMin = padding
const rangeYMax = height - padding
const rangeXMin = leftPadding
const rangeXMax = width - padding

const parseDate = timeParse('%Y-%m-%d')

const xScale = scaleTime()
	.domain([
		parseDate(data[0][0]) as Date,
		parseDate(data[data.length - 1][0]) as Date,
	])
	.range([rangeXMin, rangeXMax])

const yScale = scaleLinear()
	.domain([0, max(data, d => d[1]) as number])
	.range([rangeYMax, rangeYMin])

// Append svg to root element.
const svg = select('#app-root')
	.append('svg')
	.attr('class', 'bar-chart')
	.attr('viewBox', `0 0 ${width} ${height}`)

// Append title to svg.
svg.append('text')
	.attr('id', 'title')
	.attr('x', width / 2)
	.attr('y', 30)
	.attr('font-size', 24)
	.text('United States GDP')

svg.selectAll('.bar')
	.data(data)
	.enter()
	.append('rect')
	.attr('data-date', d => d[0])
	.attr('data-gdp', d => d[1])
	.attr('class', 'bar')
	.attr('x', d => xScale(parseDate(d[0]) as Date))
	.attr('y', d => yScale(d[1]))
	.attr('width', rangeXMax / data.length)
	.attr('height', d => rangeYMax - yScale(d[1]))
	.on('mouseenter', function () {
		const date = this.getAttribute('data-date') as string
		const gdp = this.getAttribute('data-gdp')

		select('#tooltip')
			.style('display', 'block')
			.attr('data-date', date)
			.text(`${date.slice(0, 7)}: $${gdp}B`)
	})
	.on('mouseleave', function () {
		select('#tooltip').style('display', 'none')
	})

svg.append('text')
	.attr('id', 'tooltip')
	.attr('x', width / 2)
	.attr('y', 60)
	.attr('font-size', 24)

const xAxis = axisBottom(xScale)

// Append x-axis to svg.
svg.append('g')
	.attr('id', 'x-axis')
	.attr('transform', `translate(0, ${rangeYMax})`)
	.call(xAxis)

const yAxis = axisLeft(yScale)

// Append y-axis to svg.
svg.append('g')
	.attr('id', 'y-axis')
	.attr('transform', `translate(${rangeXMin}, 0)`)
	.call(yAxis)
