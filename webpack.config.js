/* eslint-env node */
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const srcFolder = path.resolve(__dirname, 'src')

module.exports = {
	mode: 'development',
	entry: './src/main.ts',
	output: {
		filename: 'bundle.[hash].js',
		path: path.resolve(__dirname, 'dist'),
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.jsx?$/u,
				include: [srcFolder],
				use: ['babel-loader', 'eslint-loader'],
			},
			{
				test: /\.tsx?$/u,
				include: [srcFolder],
				use: ['babel-loader', 'ts-loader'],
			},
			{
				test: /\.css$/,
				include: [srcFolder],
				use: [MiniCssExtractPlugin.loader, 'css-loader'],
			},
		],
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			scriptLoading: 'defer',
			title: 'FCC Bar Chart Project',
			xhtml: true,
		}),
		new MiniCssExtractPlugin({
			filename: 'style.[hash].css',
		}),
	],
}
